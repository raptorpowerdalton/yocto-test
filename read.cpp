#include <iostream>
#include <rrd.h>

// template <typename... Args>
// static bool call_shell(Args &&...arg)
// {
//     const char *argv[sizeof...(arg) + 1] = {arg...};
//     argv[sizeof...(arg)] = nullptr;

//     pid_t fd = fork();
//     if (fd == -1)
//         return false;
//     if (fd == 0)
//     {
//         execvp(argv[0], const_cast<char *const *>(argv));
//         exit(EXIT_FAILURE);
//     }

//     int status;
//     waitpid(fd, &status, 0);

//     return WEXITSTATUS(status) == 0 ? true : false;
// }

#include <array>
// int rrd_create_r2(
//     const char *filename,
//     unsigned long pdp_step,
//     time_t last_up,
//     int no_overwrite,
//     const char **sources,
//     const char *template,
//     int argc,
//     const char **argv)
int main(int argc, char *argv[])
{
    std::cout << rrd_strversion() << '\n';
    const char* data[25] = {"DS:watts:GAUGE:300:0:24000", "RRA:AVERAGE:0.5:1:864000", NULL};

    int i = rrd_create_r2("/home/daltonovermyer/test.rrd", 100, -1, 1, NULL, NULL, 2, data);
    if(i == -1){
        std::cout << rrd_get_error() << '\n';
        std::cout << strerror(errno) << '\n';
    }
    
    return 0;
}
